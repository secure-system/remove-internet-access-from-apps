# Remove internet access from proprietary apps

Use GitLab CI to remove internet access from propritetary apps that works offline.

# Usage

> Note: it assumes the app is hosted on Google Play or other sources supported by [apkeep](https://github.com/EFForg/apkeep).

1. Fork this repository
2. Set the target `APP_ID` and your `ANDROID_KEYSTORE_FILE` environment variables in your repository CI settings
3. Trigger a CI pipeline
4. Get the clean app from pipeline artifacts
5. (Optional) Set a CI pipeline schedule to keep the app up-to-date
